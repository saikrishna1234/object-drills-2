let users = require("./1-users.cjs");

let usersWithMastersDegree = {};

Object.entries(users).map((user) => {
  if (user[1].qualification === "Masters") {
    usersWithMastersDegree[user[0]] = user[1];
  }
});

console.log(usersWithMastersDegree);
