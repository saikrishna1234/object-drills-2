let users = require("./1-users.cjs");

let groupByProgramLanguages = {};

let programGroup1 = {};
let programGroup2 = {};
let programGroup3 = {};

Object.entries(users).map((user) => {
  if (user[1].desgination.toString().includes("Javascript")) {
    programGroup1[user[0]] = user[1];
    groupByProgramLanguages["Javascript"] = programGroup1;
  } else if (user[1].desgination.toString().includes("Python")) {
    programGroup2[user[0]] = user[1];
    groupByProgramLanguages["Python"] = programGroup2;
  } else if (user[1].desgination.toString().includes("Golang")) {
    programGroup3[user[0]] = user[1];
    groupByProgramLanguages["Golang"] = programGroup3;
  }
});

console.log(groupByProgramLanguages);
