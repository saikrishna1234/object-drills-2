let users = require("./1-users.cjs");

Object.entries(users).map((user) => {
  if (user[1].desgination.includes("Senior")) {
    user[1].desginationLevel = 1;
  } else if (user[1].desgination.includes("Developer")) {
    user[1].desginationLevel = 2;
  } else if (user[1].desgination.includes("Intern")) {
    user[1].desginationLevel = 3;
  }
});

function sortByCompareMethod(a, b) {
  if (a.desginationLevel > b.desginationLevel) return 1;
  else if (a.desginationLevel < b.desginationLevel) return -1;
  else if (a.age < b.age) return 1;
  else if (a.age > b.age) return -1;
  else return 0;
}

let usersList = Object.values(users);

console.log(usersList.sort(sortByCompareMethod));
