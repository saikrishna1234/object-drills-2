let users = require("./1-users.cjs");

let usersInGermany = {};

Object.entries(users).map((user) => {
  if (user[1].nationality === "Germany") {
    usersInGermany[user[0]] = user[1];
  }
});

console.log(usersInGermany);
